import java.util.Scanner;

public class Application {
	
	public static void main (String [] args){
		Scanner reader = new Scanner(System.in);
		
		Student[] section4 = new Student[1];
		
		
		//LOOP FOR STUDENT ARRAYS HERE
		for (int i=0; i<section4.length; i++){
			
			section4[i] = new Student("Jojo", 7, 7);
			
			/* Questions for students
			
			//ask for NAME
			System.out.println("What's your name?");
			section4[i].name = reader.nextLine();
			
			//number of COURSES
			System.out.println("How many courses are you taking?");
			section4[i].numCourses = reader.nextInt();
			
			//hours of SLEEP
			System.out.println("How many hours of sleep do you get each day?");
			section4[i].sleepHrsPerDay = reader.nextInt();
			
			//EMPLOYMENT check
			System.out.println("Are you employed? Y/N");
			String answer = reader.nextLine();
			
			if(answer.equals("y") || answer.equals("Y")){
				section4[i].employed = true;
			
			} else if (answer.equals("n") || answer.equals("N")){
				section4[i].employed = false;
			
			} else {
				section4[i].employed = false;
				System.out.println("Invalid response. You are declared unemployed :P");
			}
			
			//average GRADE
			//System.out.println("What is your average grade?");
			//section4[i].avgGrade = reader.nextInt();
			*/
		
			
		}
		
		/* Lab4A PART 1 
		
		section4[section4.length-1].study();
		section4[section4.length-1].study();
		section4[section4.length-1].study();
		
		System.out.println(section4[0].amountLearnt);
		System.out.println(section4[1].amountLearnt);
		System.out.println(section4[2].amountLearnt);
		*/
		
		/* PART 2
		
		System.out.println("Enter the amount to study:");
		int amountStudied = Integer.parseInt(reader.nextLine());
		section4[section4.length-1].study(amountStudied);
		
		System.out.println(section4[section4.length-1].amountLearnt);
		*/
		
		/* PART 3
		
		//section4[0].name = "Joey"; this code line works when field is public
		
		System.out.println("Name before: " + section4[0].getName());
		section4[0].setName("Joey");
		System.out.println("Name after: " + section4[0].getName());
		
		section4[0].setNumCourses(8);
		System.out.println("Classes after: " + section4[0].getNumCourses());
		
		section4[0].setSleepHrs(4);
		System.out.println("Sleep hrs after: " + section4[0].getSleepHrs());
		
		section4[0].setEmployed(true);
		System.out.println("Employment status: " + section4[0].getEmployed());
		
		section4[0].setAvgGrade(90);
		System.out.println("Grade after: " + section4[0].getAvgGrade());
		*/
		
		// PART 4
		
		//setting first student's new average grade
		System.out.println("What is " + section4[0].getName()
			+ "'s NEW average grade?");
		int newAvgGrade = Integer.parseInt(reader.nextLine());
		section4[0].setAvgGrade(newAvgGrade);
		
		//displays results
		System.out.println("---- Results ----");
		System.out.println("Name after: " + section4[0].getName());
		System.out.println("Classes after: " + section4[0].getNumCourses());
		System.out.println("Sleep hrs after: " + section4[0].getSleepHrs());
		System.out.println("Employment status: " + section4[0].getEmployed());
		System.out.println("Grade after: " + section4[0].getAvgGrade());
		
	
	}
	
} 