
public class Student {

	//fields
	private String name;
	private int numCourses;
	private int sleepHrsPerDay;
	private boolean employed;
	private int avgGrade;
	private int amountLearnt;

	
	//constructor method
	public Student(String name, int numCourses, int sleepHrsPerDay) {
		
		this.name = name;
		this.numCourses = numCourses;
		this.sleepHrsPerDay = sleepHrsPerDay;
		this.employed = false;
		this.avgGrade = 80;
		this.amountLearnt = 0;	
		
	}

	
	//set methods
	
	public void setEmployed(boolean employed){
		this.employed = employed;
	}
	
	public void setAvgGrade(int avgGrade){
		this.avgGrade = avgGrade;
	}
	
	
	//get methods
	public String getName(){
		return this.name;
	}
	
	public int getNumCourses(){
		return this.numCourses;
	}
	
	public int getSleepHrs(){
		return this.sleepHrsPerDay;
	}
	
	public boolean getEmployed(){
		return this.employed;
	}
	
	public int getAvgGrade(){
		return this.avgGrade;
	}
	
	//study(), amountLearnt
	public void study(int amountStudied){
		
		this.amountLearnt += amountStudied;	
	}

	/*
	public void checkOccupation(){
		
		if (numCourses == 0 && employed) {
			System.out.println("You're not a Student. You're a full-time employee!");
		}
		
		else if (numCourses > 0 && employed) {
			System.out.println("Part-time student, part-time employee.");
		}
		
		else {
			System.out.println("You're a couch potato");
		}
	}
	
	public void checkLifestyle(){
		
		if (sleepHrsPerDay <= 5 && numCourses >= 8 && avgGrade >= 90) {
			System.out.println("I genuinely don't know how you're functioning but keep going I guess?");
		
		} else if (sleepHrsPerDay < 7) {
			System.out.println("You need some sleep");
			
		} else if (avgGrade <= 60) {
			System.out.println("Maybe you should study more");
			
		} else {
			System.out.println("You're doing just fine");
			
		}
		
	}
	*/

}